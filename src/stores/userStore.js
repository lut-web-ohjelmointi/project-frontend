import { defineStore } from 'pinia'

//Use userstore as a tool to save data from the currently logged in user to use further in the app
export const useUserStore = defineStore({
	id: 'user',

	state: () => ({
		loggedIn: localStorage.getItem('loggedIn'),
		username: localStorage.getItem('username')
	}),

	actions: {
		logIn(username) {
			this.loggedIn = true
			this.username = username

			localStorage.setItem('loggedIn', 'true');
			localStorage.setItem('username', username);
		},

		logOut() {
			this.loggedIn = false;
			this.username = null;

			localStorage.removeItem('loggedIn');
			localStorage.removeItem('username');
			localStorage.removeItem('userid');
			localStorage.removeItem('token');
		}
	}
})