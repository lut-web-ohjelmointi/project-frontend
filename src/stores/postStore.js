import { defineStore } from 'pinia'

//Use postStore as a tool to save the current poststore so that it can be accessed when wanting to view a single post
export const usePostStore = defineStore({
  id: 'post',

  state: () => ({
    postId: loadPostId()
  }),

  actions: {
    setPostId(id) {
      this.postId = id;
      savePostId(id);
    }
  }
});

function loadPostId() {
  return localStorage.getItem('postId') || null;
}

function savePostId(id) {
  localStorage.setItem('postId', id);
}