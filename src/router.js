import { createRouter, createWebHistory } from 'vue-router';
import { useUserStore } from '@/stores/userStore'
import Index from "@/components/Index.vue";
import PostView from "@/components/PostView.vue";
import CreatePostView from "@/components/CreatePostView.vue";
import RegisterView from "@/components/RegisterView.vue";
import LoginView from "@/components/LoginView.vue";
import Success from "@/components/Success.vue";

//Create routes that are needed for the app
const routes = [
	{
		path: "/",
		name: "Index",
		component: Index
	},
	{
		path: "/post/",
		name: "PostView",
		component: PostView,
	},
	{
		path: "/create/",
		name: "CreatePostView",
		component: CreatePostView,
		meta: {
			requiresAuth: true
		}
	},
	{
		path: "/register/",
		name: "RegisterView",
		component: RegisterView,
	},
	{
		path: "/login/",
		name: "LoginView",
		component: LoginView,
	},
	{
		path: "/success/",
		name: "Success",
		component: Success,
	}
]

const router = createRouter({
	history: createWebHistory(),
	routes
})

//If the user is already logged in, redirect away from the login page
router.beforeEach((to, from, next) => {
	const userStore = useUserStore();

	if (to.matched.some(record => record.meta.requiresAuth) && !userStore.loggedIn) {
		next('/login/');
	} else {
		next();
	}
})

export default router